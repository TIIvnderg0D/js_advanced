window.addEventListener('load', () => {
    let interval, holesSmashed, holesFailed, isRunning = false;
    const $qs = s => document.querySelector(s),
        $qsa = s => document.querySelectorAll(s);

    // ------------ Creating the field ------------- //

    const rows = document.querySelectorAll('.row');
    rows.forEach(row => {
        for (let i = 0; i < 10; i++) {
            const hole = document.createElement('td');
            hole.className = 'hole'
            row.appendChild(hole);
        }
    })

    // ----------- Initialization the Game class ----------- //
    class Game {
        constructor(difficulty) {
            this.difficulty = difficulty;
            this.holesSorted = [];
            this.increment = 0;
        }

        // --------------- Starting game function --------- //
        startGame() {
            // ----------- Clearing previous results ------------- //
            $qsa('.hole').forEach((hole, index) => {
                hole.style.backgroundColor = 'rgb(82, 38, 17)';
                hole.classList.remove('smashed');
                hole.classList.remove('failed');
                hole.dataset.status = '';
            })
            $qs('#failed').innerHTML = 'Failed: 0';
            $qs('#smashed').innerHTML = 'Smashed: 0';
            holesFailed = 0;
            holesSmashed = 0;
            this.increment = 0;
            isRunning = true;
            this.randomizeHole(this.difficulty);
        }

        // --------------- Finishing game function --------- //
        finishGame(winner) {
            isRunning = false;
            if (winner === 1) {
                alert('Congratulations, you won!');
            } else if (winner === 0) {
                alert('Sorry, you lost.\nTry again or decrease the difficulty!');
            }
            $qs('.gameStart').value = 'Start new game';
        }

        // ---------- Setting the squares to activate and fail within the chosen interval --------- //
        randomizeHole(interval) {
            // -------- Creating the basic array for all square indexes --------- //
            const basicArray = [];
            for (let i = 0; i < $qsa('.hole').length; i++) {
                basicArray.push(i);
            }
            this.holesSorted = this.shuffle(basicArray);
            // --------------- Creating the timeout activation function ----------- //
            const moleTimer = setInterval(() => {
                if (!isRunning) clearInterval(moleTimer);
                if (holesFailed >= 50) {
                    clearInterval(moleTimer);
                    this.finishGame(0);
                }
                // ---------- Getting the index of shuffled array and incrementing it ---------- //
                let holeIndex = this.getIndex();
                this.increment++;
                if ($qsa('.hole')[holeIndex]) {
                    $qsa('.hole')[holeIndex].style.backgroundColor = 'blue';
                    $qsa('.hole')[holeIndex].dataset.status = 'active';
                    // --------------- Creating the timeout fail function ----------- //
                    const fail = setTimeout(() => {
                        if (!isRunning) return;
                        if (holesFailed < 50 && $qsa('.hole')[holeIndex].dataset.status === 'active') {
                            this.setHoleStatus(holeIndex, 'red', 'failed');
                            holesFailed++;
                            $qs('#failed').innerHTML = `Failed: ${holesFailed}`;
                        }
                    }, 1300)
                    // --------------- Setting the handler for the active hole  ----------- //
                    $qsa('.hole')[holeIndex].addEventListener('click', (e) => {
                        if (e.target.dataset.status === 'active' && isRunning) {
                            clearTimeout(fail);
                            this.setHoleStatus(holeIndex, 'green', 'smashed');
                            holesSmashed++;
                            $qs('#smashed').innerHTML = `Smashed: ${holesSmashed}`;
                            if (holesSmashed >= 50) {
                                clearTimeout(fail);
                                clearInterval(moleTimer)
                                this.finishGame(1)
                            }
                        }
                    })
                }
            }, interval)
        }

        // -------------- Setting the corresponding square status ----------- //
        setHoleStatus(index, color, status) {
            $qsa('.hole')[index].style.backgroundColor = color;
            $qsa('.hole')[index].dataset.status = '';
            $qsa('.hole')[index].classList.add(status);
        }

        // ------------------------- Shuffling the array -------------------- //
        shuffle(array) {
            let j, tmp;
            for (let i = array.length - 1; i > 0; i--) {
                j = Math.floor(Math.random() * (i + 1));
                tmp = array[j];
                array[j] = array[i];
                array[i] = tmp;
            }
            return array;
        }

        // -------------------- Returning the next index -------------------- //
        getIndex() {
            return this.holesSorted[this.increment];
        }
    }
    // --------------- Creating the handler for start game button ----------- //
    $qs('.gameStart').addEventListener('click', (e) => {
        if (!interval) {
            alert('Choose the difficulty first!');
            return;
        }
        if (isRunning) return;
        const newGame = new Game(interval);
        newGame.startGame()
    })
    // --------- Creating the handler for difficulty radio buttons ---------- //
    $qs('.difficulty').addEventListener('click', (e) => {
        if (e.target.dataset) {
            interval = e.target.dataset.interval;
        }
    })
})

// ---------- First ------------ //

/*window.addEventListener('load', () => {
    if (document.cookie) {
      const cookie = document.cookie.slice(9),
            lastTime = document.createElement('h1');
      lastTime.innerHTML = `Grats, you are back! You've been here on ${cookie} last time!`;
      document.body.appendChild(lastTime);
    }
    const thisDate = new Date(),
    thisDay = prettify(thisDate.getDate()),
    thisMonth = prettify(thisDate.getMonth()),
    thisYear = thisDate.getFullYear(),
    thisHours = prettify(thisDate.getHours()),
    thisMinutes = prettify(thisDate.getMinutes()),
    thisSeconds = prettify(thisDate.getSeconds()),
    expression = `${thisDay}.${thisMonth}.${thisYear} at ${thisHours}:${thisMinutes}:${thisSeconds}`;
    document.cookie = 'lastDate='+ expression +';max-age=2592000';

    function prettify(number) {
      return number > 10 ? number : `0${number}`;
    }
})*/

// ---------- Second ------------ //

/*window.addEventListener('load', () => {
    if (document.cookie) {
        const cookies = document.cookie.split(';'),
            fontCookie = GetCookie('FontSize'),
            colorCookie = GetCookie('Color');
        document.body.style.fontSize = `${fontCookie}px`;
        document.body.style.backgroundColor = `${colorCookie}`;
    }

    function getCookieVal(offset) {
        var delimiter = document.cookie.indexOf(";", offset);
        if (delimiter == -1)
            delimiter = document.cookie.length;
        return document.cookie.substring(offset, delimiter);
    }
    function GetCookie(name) {
        const argument = name + "=",
            length = argument.length,
            cookieLength = document.cookie.length;
        let i = 0;
        while (i < cookieLength) {
            let j = i + length;
            if (document.cookie.substring(i, j) === argument)
                return getCookieVal(j);
            i = document.cookie.indexOf(" ", i) + 1;
            if (i === 0)
                break;
        }
    }
    const $ = id => document.getElementById(id);
    $('font').addEventListener('keypress', (e) => {
        if (!(e.charCode > 47 && e.charCode < 58)) {
            e.preventDefault()
        } 
    })
    $('save').addEventListener('click', () => {
        document.body.style.fontSize = `${$('font').value}px`;
        document.body.style.backgroundColor = $('color').value;
        document.cookie = `Color=${$('color').value};max-age=2592000`;
        document.cookie = `FontSize=${$('font').value};max-age=2592000`;
    })
})*/

// ---------- Third ------------ //

window.addEventListener('load', () => {
    if (document.cookie) {
        document.body.style.fontSize = `${localStorage.FontSize}px`;
        document.body.style.backgroundColor = `${localStorage.Color}`;
    }
    const $ = id => document.getElementById(id);
    $('font').addEventListener('keypress', (e) => {
        if (!(e.charCode > 47 && e.charCode < 58)) {
            e.preventDefault()
        } 
    })
    $('save').addEventListener('click', () => {
        document.body.style.fontSize = `${$('font').value}px`;
        document.body.style.backgroundColor = $('color').value;
        localStorage.Color = `${$('color').value}`;
        localStorage.FontSize = `${$('font').value}`;
    })
})
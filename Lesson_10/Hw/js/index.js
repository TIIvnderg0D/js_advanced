const $s = s => document.querySelector(s),
    $sa = sa => document.querySelectorAll(sa),
    $ce = ce => document.createElement(ce);
let mainTank, // Your tank
    enemyTanks = [],  // Enemy tanks list
    score = 0,  // Score
    isRunning = false,  // Runnning status
    mslList = [],  // Missiles list
    cliffSquares = [],  // Cliff squares
    blockedSquares = [];  // Blocked to moving squares list

window.addEventListener('load', () => {
    // ------------------- Creating the field ------------------- //
    for (let i = 0; i < 2500; i++) {
        const square = $ce('div');
        square.className = 'square';
        square.setAttribute('id', i)
        $s('.battlefield').appendChild(square);
    }

    // ----------------- Starting game function ------------------ //
    function startGame() {
        // ------- Creating tanks and setting their postions ------ //
        mainTank = new Tank(25, 48, 'top', false);
        let columnIncrement = 1;
        for (let i = 0; i < 4; i++) {
            const et = new Tank(columnIncrement, 1, 'bottom', true);
            enemyTanks.push(et);
            et.show();
            (i === 1) ? columnIncrement += 15 : columnIncrement += 16;
        }
        mainTank.show();
        pushTankSquares(mainTank)  // Blocking squares
        enemyTanks.forEach(tank => {
            pushTankSquares(tank)
        })

        // ------------- Creating some cliffs on the battlefield ------------- //
        generateEnvironment(3, 3, 5, 13);
        generateEnvironment(3, 25, 5, 20);
        generateEnvironment(27, 2, 5, 7);
        generateEnvironment(3, 25, 17, 12);
        generateEnvironment(3, 20, 25, 21);
        generateEnvironment(17, 2, 25, 16);
        generateEnvironment(8, 2, 31, 27);
        generateEnvironment(17, 2, 31, 39);
        generateEnvironment(3, 20, 45, 10);
        generateEnvironment(13, 2, 35, 5);
        generateEnvironment(3, 6, 35, 10);
        generateEnvironment(3, 6, 35, 21);

        enemiesMove();  // Starting enemy tanks moving
        isRunning = true;
    }

    // ----------------- Creating environment function ------------------ //
    function generateEnvironment(columns, rows, startC, startR) {
        const cliffs = [];
        let cliff = $ce('div');
        cliff.className = 'cliff';
        cliffs.push(cliff);
        cliff.style.gridColumnStart = startC;
        cliff.style.gridRowStart = startR;
        cliff.style.gridColumnEnd = `span ${columns}`;
        cliff.style.gridRowEnd = `span ${rows}`;
        $s('.battlefield').appendChild(cliff);
        cliffs.forEach(cliff => {  // Fill the blocking path and the blocking missiles arrays
            for (let i = 0; i < columns; i++) {
                cliffSquares.push(`${startC + i}.${startR}`)  // Array for missiles
                blockedSquares.push(`${startC + i}.${startR}`)  // Array for moving
                for (let j = 0; j < rows; j++) {
                    cliffSquares.push(`${startC + i}.${startR + j}`)
                    blockedSquares.push(`${startC + i}.${startR + j}`)
                }
            }
        })
    }

    // ----------- Blocking starting tanks squares function ----------- //
    function pushTankSquares(tank) {
        for (let i = 0; i < 3; i++) {
            blockedSquares.push(`${tank.column + i}.${tank.row}`)
            for (let j = 1; j < 3; j++) {
                blockedSquares.push(`${tank.column + i}.${tank.row + j}`);
            }
        }
    }

    // ------------------- Moving enemies function --------------------- //
    function enemiesMove() {
        enemyTanks.forEach(tank => {
            if (!tank.item) return;
            let way;
            this.cw = setInterval(() => {  // Changing the way once for 0.5sec
                way = Math.floor(Math.random() * (41 - 37) + 37);
            }, 500)
            this.mv = setInterval(() => {  //Moving each 100ms
                tank.setMovingParams(way);
            }, 100)
            this.sh = setInterval(() => {  //Shooting each 1s
                tank.shoot();
            }, 1000)
        })
    }

    // ------------------- Ending game function --------------------- //
    function gameEnd(value) {
        (value) ? alert('Round cleared.\nWell played!') : alert('Your tank was destroyed, you lost!');
        isRunning = false;
    }

    // ----------------- Clearing results function ------------------ //
    function clearResults() {
        $sa('.cannonBall, .tank, .cliff').forEach(item => {
            $s('.battlefield').removeChild(item);
        });
        mslList.forEach(msl => {
            msl.item = '';
        })
        mainTank = '';
        enemyTanks = [];
        score = 0;
        blockedSquares = [];
        $s('h3').innerHTML = `Score: ${score}`;
    }

    // -------------------- Initialising class for tanks ----------------------- //
    class Tank {
        constructor(column, row, way, enemy) {
            this.column = column;   // Tank column
            this.row = row;     // Tank row
            this.way = way;     // Tank viewing side
            this.enemy = enemy;     // is enemy tank or not
            this.item = $ce('div');     // Tank element
            this.image = $ce('img');    // Tank image
        }

        // --------------------------- Showing method ------------------------ //
        show() {
            this.item.className = 'tank';
            if (this.enemy) {       // Setting the images for tanks
                this.image.setAttribute('src', './images/enemy_tank.png');
            } else {
                this.image.setAttribute('src', './images/your_tank.png');
                this.item.classList.add('main');
            }
            this.item.appendChild(this.image);
            $s('.battlefield').appendChild(this.item);
            this.item.style.gridColumnStart = this.column;
            this.item.style.gridRowStart = this.row;
            this.item.style.display = 'block';
        }

        // ------------- Finding the position and the way of a tank ----------- //
        getPosition() {
            // ----- Bringing transform status to the same format for all browsers ---- //
            let mask = new RegExp(/((-)?\d+(\.+\d*(e-16)?(e-17)?))/g),
                transformStatus = getComputedStyle(this.image).transform.replace(mask, 0).slice(7, 13);

            // -------------------- Getting the coordinates -------------------- // 
            this.column = parseInt(getComputedStyle(this.item, null).gridColumnStart);
            this.row = parseInt(getComputedStyle(this.item, null).gridRowStart);
            switch (transformStatus) {  //  Checking image transform status and setting the way
                case '0, 1, ': this.way = 'left'; break;
                case '0, -1,': this.way = 'right'; break;
                case '-1, 0,': this.way = 'top'; break;
                case '1, 0, ': this.way = 'bottom'; break;
            }
        }

        // ------- Setting params for tank further moving depending of keyCode ------ //
        setMovingParams(code) {
            switch (code) {
                case 37: this.move('x', this.column, 90, 'dec'); break;
                case 38: this.move('y', this.row, 180, 'dec'); break;
                case 39: this.move('x', this.column, 270, 'inc'); break;
                case 40: this.move('y', this.row, 0, 'inc'); break;
            }
        }

        // ------------------------- Moving tank method ------------------------- //
        move(axis, value, rotation, increment) {
            if (!isRunning || !this.item) return;
            this.image.style.transform = `rotate(${rotation}deg)`;  // Rotating the image

            if (value === 1 && increment === 'dec' || value === 48 && increment === 'inc') return;  // Checking if not the edges of the field
            
            if (!this.pathClearStatus(axis, value, increment)) return;  // Checking if path is clear
            increment === 'inc' ? value++ : value--;
            // --- Moving depending of the axis and increment value, blocking & clearing squares --- //
            switch (axis) {
                case 'y': {
                    this.item.style.gridRowStart = value;
                    this.getPosition();
                    if (increment === 'inc') {
                        for (let i = 0; i < 3; i++) {
                            blockedSquares.push(`${this.column + i}.${this.row + 2}`);
                            blockedSquares.splice(blockedSquares.indexOf(`${this.column + i}.${this.row - 1}`), 1);
                        }
                        break;
                    } else {
                        for (let i = 0; i < 3; i++) {
                            blockedSquares.push(`${this.column + i}.${this.row}`);
                            blockedSquares.splice(blockedSquares.indexOf(`${this.column + i}.${this.row + 3}`), 1);
                        }
                        break;
                    }
                };
                case 'x': {
                    this.item.style.gridColumnStart = value;
                    this.getPosition();
                    if (increment === 'inc') {
                        for (let i = 0; i < 3; i++) {
                            blockedSquares.push(`${this.column + 2}.${this.row + i}`);
                            blockedSquares.splice(blockedSquares.indexOf(`${this.column - 1}.${this.row + i}`), 1);
                        }
                        break;
                    } else {
                        for (let i = 0; i < 3; i++) {
                            blockedSquares.push(`${this.column}.${this.row + i}`);
                            blockedSquares.splice(blockedSquares.indexOf(`${this.column + 3}.${this.row + i}`), 1);
                        }
                        break;
                    }
                }
            }
        }

        // ------------------------- Checking path method ------------------------- //
        pathClearStatus(axis, value, increment) {
            let isClear = true;
            // ------- Checking if next 3 ones are not in the blocked squares list --- //
            if (axis === 'y' && increment === 'dec') {
                for (let i = 0; i < 3; i++) {
                    if (blockedSquares.includes(`${this.column + i}.${value - 1}`)) isClear = false;
                }
            } else if (axis === 'y' && increment === 'inc') {
                for (let i = 0; i < 3; i++) {
                    if (blockedSquares.includes(`${this.column + i}.${value + 3}`)) isClear = false;
                }
            } else if (axis === 'x' && increment === 'dec') {
                for (let i = 0; i < 3; i++) {
                    if (blockedSquares.includes(`${value - 1}.${this.row + i}`)) isClear = false;
                }
            } else if (axis === 'x' && increment === 'inc') {
                for (let i = 0; i < 3; i++) {
                    if (blockedSquares.includes(`${value + 3}.${this.row + i}`)) isClear = false;
                }
            }
            return isClear;
        }

        // ------------------------ Shooting tank method ------------------------ //
        shoot() {
            if (!enemyTanks.includes(this) && this !== mainTank) return;
            let msl;
            this.getPosition();  // Getting the shoot direction
            if ((this.column === 48 && this.way === 'right') ||
                (this.row === 48 && this.way === 'bottom')) return;
            // -------- Creating new missile depending of fighting force -------- //
            msl = new Missile(this.column, this.row, this.way, (this === mainTank) ? true : false);
            mslList.push(msl)
            msl.show();
            msl.fly();
        }
    }

    // ----------------- Initialising class for missiles  ------------------ //
    class Missile {
        constructor(column, row, way, allied) {
            this.column = column;   // Missile column
            this.row = row;     // Missile row
            this.way = way;     // Missile way
            this.allied = allied;  // Is shoot by main tank or not
            this.item = $ce('div');  // DOM element
        }

        // ----------------- Showing method ------------------ //
        show() {
            const image = $ce('img');
            this.item.className = 'cannonBall';
            image.setAttribute('src', './images/cannon_ball.svg');
            this.item.appendChild(image);
            $s('.battlefield').appendChild(this.item);
            this.item.style.gridColumn = this.column;
            this.item.style.gridRow = this.row;
            this.item.style.display = 'block';
        }

        // ----------------- Flying method ------------------ //
        fly() {
            switch (this.way) {     // Getting the start position at the tanks front side
                case 'left': this.column--; this.row++; break;
                case 'right': this.column += 3; this.row++; break;
                case 'top': this.column++; this.row--; break;
                case 'bottom': this.column++; this.row += 3; break;
            }
            if (cliffSquares.includes(`${this.column}.${this.row}`)) {  // In case of shooting right into the cliff
                this.deleteMissile();
                return;
            }

            this.item.style.gridColumn = this.column;  // Setting the image on start position
            this.item.style.gridRow = this.row;

            const flying = setInterval(() => {  // Setting the interval function
                if (!this.item || !mainTank) clearInterval(flying);
                switch (this.way) {  // In case of cliffs on the way, break it. Else flying forward
                    case 'top': {
                        if (cliffSquares.includes(`${this.column}.${this.row - 1}`)) {
                            this.deleteMissile();
                            return;
                        }
                        this.row--;
                        break;
                    }
                    case 'bottom': {
                        if (cliffSquares.includes(`${this.column}.${this.row + 1}`)) {
                            this.deleteMissile();
                            return;
                        }
                        this.row++;
                        break;
                    }
                    case 'left': {
                        if (cliffSquares.includes(`${this.column - 1}.${this.row}`)) {
                            this.deleteMissile();
                            return;
                        }
                        this.column--;
                        break;
                    }
                    case 'right': {
                        if (cliffSquares.includes(`${this.column + 1}.${this.row}`)) {
                            this.deleteMissile();
                            return;
                        }
                        this.column++;
                        break;
                    }
                }

                if (this.row > 50 || this.column > 50
                    || this.column < 1 || this.row < 1) {  // In case of reaching the end of the field
                    clearInterval(flying);
                    if (this.item && mainTank) {
                        $s('.battlefield').removeChild(this.item);
                        mslList.splice(this, 1)
                    }
                }

                enemyTanks.forEach((tank, index) => {
                    if (this.checkReaching(tank) && this.allied) {  // In case of reaching enemy tank
                        this.destroy(index);
                    }
                })

                if (this.item) {
                    this.item.style.gridColumn = this.column;
                    this.item.style.gridRow = this.row;
                }

                if (!mainTank) return
                mainTank.getPosition();
                if (this.checkReaching(mainTank) && !this.allied) {  // In case of reaching main tank
                    clearInterval(flying);
                    gameEnd(false);
                    clearResults();
                }
            }, 40)
        }

        // ----------------- Checking of hitting tank method ------------------ //
        checkReaching(tank) {
            let gotcha = false;
            for (let i = 0; i < 3; i++) {
                if (tank.column + i === this.column && tank.row === this.row) gotcha = true;
                for (let j = 0; j < 3; j++) {
                    if (tank.column + i === this.column && tank.row + j === this.row) gotcha = true;
                }
            }
            return gotcha;
        }

        // ----------------- Removing missile method ------------------ //
        deleteMissile() {
            if (this.item && mainTank) {
                $s('.battlefield').removeChild(this.item);
                this.item = '';
                mslList.splice(this, 1);
            }
        }

        // ----------------- Destroying enemies method ------------------ //
        destroy(tankIndex) {
            const eTank = enemyTanks[tankIndex];
            eTank.item.style.display = 'none';
            eTank.item = '';
            this.deleteMissile();
            score++;
            $s('h3').innerHTML = `Score: ${score}`;
            enemyTanks.splice(tankIndex, 1)  // Removing tank from current alive tanks list
            for (let i = 0; i < 3; i++) {  // Clearing squares of destroyed tank
                blockedSquares.splice(blockedSquares.indexOf(`${eTank.column + i}.${eTank.row}`), 1);
                for (let j = 1; j < 3; j++) {
                    blockedSquares.splice(blockedSquares.indexOf(`${eTank.column + i}.${eTank.row + j}`), 1);
                }
            }
            if (enemyTanks.length === 0) {
                gameEnd(true);
            }
        }
    }

    // -------- Setting the handler for game start button -------- //
    $s('.start').addEventListener('click', () => {
        if (isRunning) return;
        clearResults();
        startGame();
    })

    // ---------- Setting the handler for key pressing --------------- //
    window.addEventListener('keydown', (e) => {
        if (!mainTank) return;
        if (e.keyCode >= 37 && e.keyCode <= 40) {
            mainTank.setMovingParams(e.keyCode);
        } else if (e.keyCode === 32) {
            mainTank.shoot();
        }
    })

    // ------------ Fixing space from triggering buttons ------------ //
    window.addEventListener('keyup', (e) => {
        if (e.keyCode === 32) {
            e.preventDefault();
        }
    })
})

window.addEventListener('load', () => {
    const planetsWrapper = document.querySelector('.planets');
    // ------------- Creating a request ------------ //
    function setPromise(url) {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {  // In case of response
                    if (xhr.status == 200) {  // In case of success
                        resolve(xhr.response);
                    } else {  // In case of error
                        let error = new Error(this.statusText);
                        error.code = this.status;
                        reject(error);
                    }
                }
            };
            xhr.onerror = function () {  // In case of network error
                reject(new Error("Network Error"));
            };
            xhr.send();
        })
    }
    // ------------- Getting the planets data ----------- //
    setPromise("https://swapi.co/api/planets/")
        .then(response => getPlanets(response),
            error => console.log(`Rejected:`, error)
        )
        .then(() => {  // Setting a loader
            let preloaderEl = document.getElementById('preloader');
            preloaderEl.classList.add('hidden');
            preloaderEl.classList.remove('visible');
        })

    // -------------- Planets data resolve function ---------- //
    function getPlanets(data) {
        const planetsData = JSON.parse(data);
        planetsData.results.forEach(planet => {
            if (planet.residents.length !== 0) {
                planet.residents.forEach(res => {
                    setPromise(res)     // Getting the inhabitants data
                        .then(response => {  // Creating dom elements for each one
                            const data = JSON.parse(response);
                            const residentName = document.createElement('span');
                            residentWrapper = document.createElement('div');
                            residentWrapper.className = 'residents';
                            residentName.innerHTML = data.name;
                            residentWrapper.appendChild(residentName);
                            planetItem.appendChild(residentWrapper);
                        })
                        .catch(err => { console.error(err); });  // Catching errors
                })
            }
            // ----------------- Creating the elements for other properties ----------- //
            const planetName = document.createElement('span'),
                planetClimate = document.createElement('span'),
                planetTerrain = document.createElement('span'),
                planetItem = document.createElement('div');
            planetItem.className = 'item';
            planetName.className = 'name';
            planetItem.appendChild(planetName);
            planetItem.appendChild(planetClimate);
            planetItem.appendChild(planetTerrain);
            planetsWrapper.appendChild(planetItem)
            planetName.innerHTML = `Name: ${planet.name}`;
            planetClimate.innerHTML = `Climate: ${planet.climate}`;
            planetTerrain.innerHTML = `Terrain: ${planet.terrain}`;
        })
    }
})


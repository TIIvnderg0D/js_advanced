window.addEventListener('load', () => {
    const planetsWrapper = document.querySelector('.planets');

    // ------------- Creating a request ------------ //
    let url = 'https://swapi.co/api/planets/',
        options = { method: 'GET', },
        request = fetch(url, options);

    // --------------- Resolved request function ----------- //
    function getPlanets(data) {
        const planetsData = data.results;
        planetsData.forEach(planet => {  // Getting residents links
            if (planet.residents.length !== 0) {  // if residents exist
                planet.residents.forEach(res => {  // creating resident name span for each link
                    let urlNew = res;
                        fetch(urlNew, options)
                        .then(response => response.json())
                        .then(data => {
                            const residentName = document.createElement('span');
                            residentWrapper = document.createElement('div');
                            residentWrapper.className = 'residents';
                            residentName.innerHTML = data.name;
                            residentWrapper.appendChild(residentName);
                            planetItem.appendChild(residentWrapper);
                        })
                        .catch(err => { console.error(err); });
                })
                
            }
            // ----------- Creating elements for each property ------------- //
            const planetName = document.createElement('span'),
                planetClimate = document.createElement('span'),
                planetTerrain = document.createElement('span'),
                planetItem = document.createElement('div');
            planetItem.className = 'item';
            planetName.className = 'name';
            planetItem.appendChild(planetName);
            planetItem.appendChild(planetClimate);
            planetItem.appendChild(planetTerrain);
            planetsWrapper.appendChild(planetItem)
            planetName.innerHTML = `Name: ${planet.name}`;
            planetClimate.innerHTML = `Climate: ${planet.climate}`;
            planetTerrain.innerHTML = `Terrain: ${planet.terrain}`;
        })
    }

    // ------------ Fetch results ------------- //
    request
    .then(response => response.json())
    .then(data => {
        let preloaderEl = document.getElementById('preloader');
        preloaderEl.classList.add('hidden');
        preloaderEl.classList.remove('visible');
        getPlanets(data)
    })

    .catch(err => { console.error(err); });
})
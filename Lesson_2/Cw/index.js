
let seconds = 0 , minutes = 0, hours = 0, intervalSeconds, intervalMinutes, intervalHours; 

const get = (id) => document.getElementById(id);

const countSeconds = () => {
    get("output").innerHTML = `${accurate(hours)}:${accurate(minutes)}:${accurate(seconds)}`;
    ++seconds;
    seconds > 59 ? seconds = 0 : seconds; 
}
const countMinutes = () => {
    ++minutes;
    minutes > 59 ? minutes = 0 : minutes; 
}
const countHours = () => {
    ++hours;
    hours > 23 ? hours = 0 : hours; 
}

function accurate(interval) {
   return (interval < 10) ? `0${interval}` : interval;
}

get("startButton").onclick = () => {
    intervalSeconds = setInterval(countSeconds, 1);
    intervalMinutes = setInterval(countMinutes , 1000);
    intervalHours = setInterval(countHours, 60000);
}

get("pauseButton").onclick = () => {
    clearInterval(intervalSeconds);
    clearInterval(intervalMinutes);
    clearInterval(intervalHours);
}
get("resetButton").onclick = () => {
    clearInterval(intervalSeconds);
    clearInterval(intervalMinutes);
    clearInterval(intervalHours);
    hours = 0;
    minutes = 0;
    seconds = 0;
    get("output").innerHTML = `${accurate(hours)}:${accurate(minutes)}:${accurate(seconds)}`;
}


function User(login, password, url) {
    this.login = login;
    this.password = password;
    this.url = url;
}

const dima = new User('dimaTop', '12345', "https://dimaTop");
const misha = new User('misha123', '123456', "https://Misha123");
const pasha = new User('gamerPasha','123453', "https://gamerPasha");
const vova = new User('VovaBest', '12345124', "https://VovaBest");
const peter = new User('Peter89', '1234512', "https://Peter89");
const alex = new User('AlexNum1', '1234512', "https://AlexNum1");
const ivan = new User('lonelyGuy', '1234565', "https://lonelyGuy");
const kolya = new User('Kolya04', '1234597', "https://Kolya04");
const max = new User('xxGodxx', '1234597', "https://xxGodxx");
const sanya = new User('Sasha95', '1234543', "https://Sasha95");
const Admin = Object.create(dima);
console.log(Admin);


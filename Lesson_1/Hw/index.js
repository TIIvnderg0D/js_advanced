
//------- First --------//

function Human(name, surname, age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
}

const people = [];

people[0] = new Human('John', 'Smith', 30);
people[1] = new Human('James', 'Potter', 25);
people[2] = new Human('Jane', 'Air', 35);
people[3] = new Human('Michael', 'Fergusson', 41);
people[4] = new Human('Sara', 'Connor', 43);
people[5] = new Human('Scott', 'Tompson', 23);

function sortHumansByAge(humanFirst, humanSecond) {
    if (humanFirst.age > humanSecond.age) {
        return 1
    }
    if (humanFirst.age < humanSecond.age) {
        return -1
    } else {
        return 0;
    }
}

people.sort(sortHumansByAge);
console.log(people)

//--------- Second ---------//


function NewHuman(name, surname) {
    this.name = name;               //свойства экземпляра
    this.surname = name;            //свойства экземпляра
}

NewHuman.age = 35  // свойство конструктора

NewHuman.prototype.getName = function() {
    console.log(name);     //метод екземпляра
} 

NewHuman.getSurname = function() {
    console.log(surname)   //метод конструктора
}


//--------- Third ---------//

let value1 = 123;
console.log(typeof value1);         //number
let value2 = value1.toString();
console.log(typeof value2);         //string

//в обьектах:

const today = new Date();  //  Date --> Object.prototype --> toString()
console.log(today);  

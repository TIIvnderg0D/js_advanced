window.addEventListener('load', () => {

    // ------------- Creating general request ------------ //
    const xhr = new XMLHttpRequest(),
        wrapper = document.querySelector('.wrapper');
    xhr.open("GET", "https://swapi.co/api/people/", true);
    let preloaderEl = document.getElementById('preloader');
    preloaderEl.classList.add('hidden');
    preloaderEl.classList.remove('visible');
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {  // In case of response
            if (xhr.status == 200) {  // In case of success
                const characters = JSON.parse(xhr.responseText);  // parse the result
                characters.results.forEach(char => {  // creating elements for each property and setting values for them
                    const item = document.createElement('li'),
                        itemName = document.createElement('span'),
                        itemGender = document.createElement('span'),
                        itemHome = document.createElement('span');
                    item.className = 'item';
                    itemName.innerHTML = `Name: ${char.name}`;
                    itemGender.innerHTML = `Gender: ${char.gender}`;
                    //------------------ Creating request for planets and setting planet element value -------------- //
                    const plXhr = new XMLHttpRequest();
                    plXhr.open("GET", char.homeworld, true);
                    plXhr.onreadystatechange = () => {
                        if (plXhr.readyState == 4) {  // In case of response
                            if (plXhr.status == 200) {  // In case of success
                                const planetsText = JSON.parse(plXhr.responseText);
                                itemHome.innerHTML = `Homeworld: ${planetsText.name}`;
                            }
                        }
                    }
                    plXhr.send();
                    item.appendChild(itemName);
                    item.appendChild(itemGender);
                    item.appendChild(itemHome); 
                    const ships = char.starships;
                    // ---------------- In case of piloting a ship --------------- //
                    if (ships.length !== 0) {
                        const showShips = document.createElement('button');
                        showShips.innerHTML = 'Ships list';
                        showShips.addEventListener('click', (e) => {  // Creating a handler for 'show ships' button
                            piloted = document.createElement('h3');
                            piloted.innerHTML = 'Piloted ships:';
                            item.replaceChild(piloted, showShips); // Replacing a button
                            ships.forEach(ship => {
                                //------------------ Creating request for spaceships and element for ship value -------------- //
                                const shpXhr = new XMLHttpRequest();
                                shpXhr.open("GET", ship, true);
                                preloaderEl.classList.add('hidden');
                                preloaderEl.classList.remove('visible');
                                shpXhr.onreadystatechange = () => {
                                    if (shpXhr.readyState == 4) {  // In case of response
                                        if (shpXhr.status == 200) {  // In case of success
                                            const shipsText = JSON.parse(shpXhr.responseText);
                                            const shipsList = [];
                                            shipsList.push(shipsText);
                                            shipsList.forEach(ship => {
                                                shipItem = document.createElement('span');
                                                shipItem.className = 'ship';
                                                shipItem.innerHTML = ship.name;
                                                item.appendChild(shipItem);
                                            })
                                        }
                                    }
                                }
                                shpXhr.send();
                            }) 
                        })
                        item.appendChild(showShips);
                    }
                    wrapper.appendChild(item);
                });
            } else {  // In case of error
                let error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        }
    }
    xhr.send();
    xhr.onerror = function () {  // In case of network error
        reject(new Error("Network Error"));
    };
})
/*window.addEventListener('load', () => {
    const xhr = new XMLHttpRequest(),
        currencyBlock = document.querySelector('.currency');
    xhr.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json", true); 
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {  // Если был получен ответ
            if (xhr.status == 200) {  // Если удачно
                const currencies = JSON.parse(xhr.responseText);  // парсим результат запроса
                currencies.forEach(currency => {  // создаем разметку и присваиваем значения елементам
                    let item = document.createElement('li'),
                        itemName = document.createElement('span'),
                        itemValue = document.createElement('span');
                    item.className = 'item';
                    itemName.innerHTML = currency.cc;
                    itemValue.innerHTML = parseFloat(currency.rate).toFixed(2);  // округляем до .2 десятых
                    item.appendChild(itemName);
                    item.appendChild(itemValue);
                    currencyBlock.appendChild(item);
                });
            }
        }
    }
    xhr.send();
})*/

window.addEventListener('load', () => {
    const getButton = document.querySelector('.getValues');
    getButton.addEventListener('click', () => {
        const xhr = new XMLHttpRequest(),
        currencyBlock = document.querySelector('.currency');
    xhr.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json", true); 
    xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {  // Если был получен ответ
            if (xhr.status === 200) {  // Если удачно
                const currencies = JSON.parse(xhr.responseText),  // парсим результат запроса
                    hugeCurrencies = currencies.filter(value => {
                        return parseInt(value.rate) > 25;
                    }) 
                hugeCurrencies.forEach(currency => {  // создаем разметку и присваиваем значения елементам
                    let item = document.createElement('li'),
                        itemName = document.createElement('span'),
                        itemValue = document.createElement('span');
                    item.className = 'item';
                    itemName.innerHTML = currency.cc;
                    itemValue.innerHTML = parseFloat(currency.rate).toFixed(2);  // округляем до .2 десятых
                    item.appendChild(itemName);
                    item.appendChild(itemValue);
                    currencyBlock.appendChild(item);
                });
            }
            if (xhr.status >= 400) {  // если ошибка
                const errorElem = document.createElement('p');
                errorElem.innerHTML = 'Oops, что то пошло не так. Произошла ошибка';
                body.appendChild(errorElem);
            }
        }
    }
    xhr.send();
    })
})
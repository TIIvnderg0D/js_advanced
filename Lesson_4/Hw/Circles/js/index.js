const button = document.querySelector('button'),
    body = document.querySelector('body'),
    drawButton = document.createElement('button')
let diameter = '';

window.onload = () => {
    button.removeAttribute('hidden')
    button.onclick = getDiameter;
    drawButton.innerHTML = 'Draw!';
    button.after(drawButton)
    drawButton.onclick = drawCircles;
    circleBox = document.createElement('div');
    circleBox.classList.add('circleBox');
    body.appendChild(circleBox);
    
    circleBox.addEventListener('click', (e) => {
        try {
            circleBox.removeChild(e.target)
        } catch (e) {
            alert('Aim the circles only!')
        }
    })
}

function getDiameter() {
    diameter = parseInt(prompt('Enter diameter', diameter))
    return diameter
}

function drawCircles() {
    if (!diameter) {
        alert('Diameter is undefined')
    }
    else {
        circleBox.style.maxWidth = 10 * diameter + 'px'
        circleBox.style.maxHeight = 10 * diameter + 'px'
        for (let i = 0; i < 100; i++) {
            const circle = document.createElement('div')
            circle.classList.add('circle')
            color = [0, 0, 0].map(e => {
                return e = Math.round(Math.random() * 255 + 1)
            });
            circle.style.width = diameter + 'px';
            circle.style.height = diameter + 'px';
            circle.style.backgroundColor = `rgb(${color[0]}, ${color[1]}, ${color[2]})`;
            circleBox.appendChild(circle)
        }
    }
}


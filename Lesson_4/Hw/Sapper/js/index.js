const root = document.querySelector('.root'),
    field = document.querySelector('.field'),
    start = document.querySelector('.start'),
    minesCounter = document.querySelector('.minesCounter'),
    input = document.querySelector('.size');
let size, minesCount, firstClick = true, defused = 0, flagsSet = 0, isDead = false;

window.addEventListener('DOMContentLoaded', (event) => {
    start.addEventListener('click', () => {
        generateNumbers(generateMines());
    })
    // ---------- Click ------------//
    field.addEventListener('click', (e) => {
        if (isDead) return
        if (firstClick) {
            const newGame = document.createElement('button');
            newGame.innerHTML = 'Start a new game'
            root.insertBefore(newGame, minesCounter)
            newGame.addEventListener('click', () => {
                isDead = false;
                defused = 0;
                flagsSet = 0;
                generateNumbers(generateMines());
            })
            firstClick = false
        }
        if (e.target.firstChild && e.target.childElementCount === 1 || e.target.tagName === 'SPAN') {
            if (e.target.firstChild.className === 'bomb') {
                lose();
                return
            } else {
                if (e.target.tagName !== 'SPAN') {
                    openNumbers(e.target)
                }
            }
        } else {
            if (e.target.className !== 'flag') {
                id = parseInt(e.target.dataset.id)
                neighbours = getNeighbours(id)
                neighbourSquares = [].filter.call(squares, (square, index) => neighbours.includes(index))
                neighbourSquares.forEach(square => {
                    if (square.firstChild && square.firstChild.tagName === 'SPAN') {
                        if (square.lastChild.tagName !== 'IMG') {
                            openNumbers(square)
                        }
                    } else {
                        if (!square.firstChild || square.firstChild.tagName !== 'IMG') {
                            square.style.backgroundColor = 'white';
                            square.setAttribute('data-status', 'opened')
                        }
                    }
                })
                e.target.style.backgroundColor = 'white';
                e.target.setAttribute('data-status', 'opened')
            }
        }
    })
    // ---------- Right Click ------------//
    field.addEventListener('contextmenu', (e) => {
        if (isDead) return
        e.preventDefault();
        const flag = document.createElement('img');
        flag.className = 'flag';
        flag.setAttribute('src', './images/flag.png')
        if (!e.target.firstChild) {
            if (e.target.className === 'flag') {
                if (e.target.parentNode.firstChild.className === 'bomb') {
                    defused--
                }
                e.target.parentNode.removeChild(e.target)
                flagsSet--
            } else {
                if (flagsSet !== minesCount && e.target.dataset.status !== 'opened') {
                    e.target.appendChild(flag);
                    flagsSet++
                }
            }
        } else {
            if (flagsSet !== minesCount) {
                if (e.target.dataset.status !== 'opened') {
                    e.target.appendChild(flag);
                    flagsSet++
                }
                if (e.target.firstChild.className === 'bomb') {
                    defused++
                    if (defused === minesCount) {
                        win();
                        isDead = true
                    }
                }
            }
        }
        countFlags()
    })
    // ---------- Double Click ------------//
    field.addEventListener('dblclick', (e) => {
        if (isDead) return
        if (e.target.tagName === 'SPAN') {
            id = parseInt(e.target.parentNode.dataset.id)
            neighbours = getNeighbours(id)
            neighbourSquares = [].filter.call(squares, (square, index) => neighbours.includes(index))
            defusedList = neighbourSquares.filter(square => {
                return square.lastChild && square.lastChild.className === 'flag'
            })
            if (parseInt(e.target.textContent) === defusedList.length) {
                neighbourSquares.forEach(item => {
                    if (!item.firstChild) {
                        item.style.backgroundColor = 'white';
                        item.setAttribute('data-status', 'opened')
                    } else {
                        if (item.firstChild.className === 'bomb' && item.lastChild.className !== 'flag') {
                            lose();
                            return
                        } else {
                            openNumbers(item)
                        }
                    }
                })
            }
        } else return
    })
})

// ---------- Opening items with bomb count ------------//
function openNumbers(item) {
    if (item.firstChild && item.firstChild.tagName === 'SPAN') {
        switch (item.firstChild.textContent) {
            case '1': item.style.backgroundColor = 'lime'; break;
            case '2': item.style.backgroundColor = 'yellow'; break;
            case '3': item.style.backgroundColor = 'orange'; break;
            case '4': item.style.backgroundColor = 'brown'; break;
            case '5': item.style.backgroundColor = 'red'; break;
            case '6': item.style.backgroundColor = 'maroon'; break;
            case '7': item.style.backgroundColor = 'darkRed'; break;
            case '8': item.style.backgroundColor = 'darkSlateGray'; break;
        }
        item.firstChild.setAttribute('data-status', 'opened')
        item.firstChild.style.display = 'block';
    }
    if (!item.firstChild) {
        item.style.backgroundColor = 'white';
        item.setAttribute('data-status', 'opened')
    }
}

// ---------- Getting indexes of item's neighbours ------------//
function getNeighbours(item) {
    const value = parseInt(input.value);
    let neighbours = [], leftBorders = [], rightBorders = []
    for (let k = 0; k < size; k++) {
        if (Number.isInteger(k / value)) {
            leftBorders.push(k);
            rightBorders.push(k + value - 1);
        }
    }
    if (leftBorders.includes(item)) {
        neighbours = [item + 1, item - value, item - value + 1, item + value, item + value + 1];
    } else if (rightBorders.includes(item)) {
        neighbours = [item - 1, item - value - 1, item - value, item + value - 1, item + value];
    } else {
        neighbours = [item + 1, item - 1, item - value - 1, item - value, item - value + 1, item + value - 1, item + value, item + value + 1];
    }
    return neighbours
}

function countFlags() {
    minesCounter.innerHTML = `Flags: ${flagsSet}/${minesCount}`;
}

function win() {
    alert('Congratulations, Your Majesty!\n\rThe victory is Yours!')
}

function lose() {
    bombs = document.querySelectorAll('.bomb')
    flags = document.querySelectorAll('.flag')
    bombs.forEach(bomb => {
        bomb.style.display = 'block'
    })
    flags.forEach(flag => {
        flag.style.display = 'none'
    })
    alert('BOOM! Nice try\n\rWasted!')
    isDead = true;
}

// ---------- Setting numbers for the bomb counters ------------//
function generateNumbers(minedIndexes) {
    squares = document.querySelectorAll('.square');
    const value = parseInt(input.value);
    let neighboursOfNeighbours = [];
    minedIndexes.forEach(item => {
        neighbours = getNeighbours(item);
        neighbours.forEach(item => {
            neighboursOfNeighbours = getNeighbours(item);
            const finalNeighbours = neighboursOfNeighbours.filter((index) => {
                return minedIndexes.includes(index)
            })
            if (squares[item] && !minedIndexes.includes(item)) {
                const bombsNear = document.createElement('span');
                if (!squares[item].hasChildNodes()) {
                    squares[item].appendChild(bombsNear)
                    bombsNear.innerHTML = finalNeighbours.length;
                }
                return squares;
            }
        })
    })
    countFlags();
}

// ---------- Creating bombs on the field ------------//
function generateMines() {
    while (field.firstChild) {
        field.removeChild(field.firstChild);
    }
    if (input.value) {
        size = Math.pow(input.value, 2)
        minesCount = Math.round(size / 6);
        start.disabled = 'true';
    } else {
        alert('Size is undefined')
    }
    for (let i = 0; i < size; i++) {
        const square = document.createElement('div')
        square.classList.add('square')
        square.setAttribute('data-id', i);
        field.style.width = input.value * 50 + 'px';
        field.style.height = input.value * 50 + 'px';
        field.appendChild(square)
    }
    const minedIndexes = [],
        squares = document.querySelectorAll('.square')
    while (minedIndexes.length < minesCount) {
        const randomItem = Math.round(Math.random() * (size - 1))
        if (minedIndexes.indexOf(randomItem) > -1) continue;
        minedIndexes.push(randomItem)
    }
    const minedSquares = [].filter.call(squares, (item, index) => minedIndexes.includes(index))
    minedSquares.forEach(elem => {
        const bomb = document.createElement('img');
        bomb.className = 'bomb';
        bomb.setAttribute('src', './images/naval-mine.jpg')
        elem.appendChild(bomb);
    })
    return minedIndexes
}

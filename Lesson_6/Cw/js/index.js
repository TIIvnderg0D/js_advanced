const root = document.querySelector('.table-wrapper'),
    size = document.querySelector('.size'),
    newPizza = document.querySelector('.new'),
    ings = document.querySelectorAll('.ingredients img'),
    table = document.querySelector('.table'),
    banner = document.querySelector('.banner'),
    chosenSize = document.querySelector('#size'),
    chosenSauce = document.querySelector('#sauce'),
    ingredientsWrapper = document.querySelector('.ingr-wrapper'),
    priceResult = document.querySelector('#price'),
    resetButton = document.getElementById('resetButton'),
    submitButton = document.getElementById('submitButton'),
    form = document.forms[0];

// --------- Setting default sizes ---------//

let sizes = 150,
    ingredients = 0,
    sauces = 0;

// --------- Initialization Pizza Class ---------//

class Pizza {
    constructor(sizes, ingredients, sauces) {
        this.sizes = sizes;
        this.ingredients = ingredients;
        this.sauces = sauces;
    }

    getPrice() {
        return this.sizes + this.ingredients + this.sauces
    }
}

// --------- Creating drag simulation for ingredients and sauces ---------//

ings.forEach(ing => {
    ing.addEventListener('mousedown', (ev) => {
        let movingOne = ev.target.cloneNode();
        const sauce = ev.target.dataset.sauce,
            ingredient = ev.target.dataset.ingredient,
            priceValue = ev.target.dataset.price;
        movingOne.style.position = 'absolute';
        movingOne.style.zIndex = 10;
        movingOne.style.cursor = 'move'
        root.appendChild(movingOne)
        moveAt(ev.pageX, ev.pageY);

        // --------- Simulating moving of the element ---------//

        function moveAt(pageX, pageY) {
            movingOne.style.left = pageX - movingOne.offsetWidth / 2 + 'px';
            movingOne.style.top = pageY - movingOne.offsetHeight / 2 + 'px';
        }

        function onMouseMove(event) {
            moveAt(event.pageX, event.pageY);

            movingOne.hidden = true;
            let elemBelow = document.elementFromPoint(event.clientX, event.clientY);   // Getting needed element below 
            movingOne.hidden = false;
            if (!elemBelow) return;
            if (elemBelow.parentElement !== table) {
                movingOne.onmouseup = function () {
                    root.removeChild(movingOne)  // Removing element while not above target
                }
            } else {
                // --------- Setting values and images after mouseup ---------//
                movingOne.onmouseup = function () {
                    if (ingredient) {    // incrementing ingredients 
                        ingredients += parseInt(priceValue)
                        const chosenIngredient = document.createElement('input');
                        ingredientsWrapper.appendChild(chosenIngredient)
                        chosenIngredient.value = movingOne.dataset.ingredient;
                    }
                    if (sauce) {    // replacing the sauce
                        let existingSauce = document.querySelector('.dropped[data-sauce]')
                        if (existingSauce) {
                            table.replaceChild(movingOne, existingSauce)
                        }
                        sauces = parseInt(priceValue)
                        chosenSauce.value = movingOne.dataset.sauce;
                    }

                    // --------- Resetting the tracing element---------//
                    document.removeEventListener('mousemove', onMouseMove);
                    movingOne.onmouseup = null;
                    movingOne.className = 'dropped';
                    table.appendChild(movingOne);
                    movingOne = '';
                    createNewPizza()
                };
            }
        }
        document.addEventListener('mousemove', onMouseMove);
    })
})

// --------- Setting event handler for sizes ---------//

size.addEventListener('click', (e) => {
    const size = e.target.dataset.size,
        priceValue = e.target.dataset.price;
    if (size) {
        chosenSize.value = size;
        sizes = parseInt(priceValue)
    };
    createNewPizza()
})

// --------- Simulating banner running from user ---------//

banner.addEventListener('mousemove', (e) => {
    e.target.style.left = Math.random() * 75 + "vw";
    e.target.style.top = Math.random() * 85 + "vh";
})

// --------- Resetting the values for new Pizza ---------//

newPizza.addEventListener('click', () => {
    ingredients = 0, sauces = 0, sizes = 150,
        chosenSize.value = '',
        chosenSauce.value = '';
    chosenSize.value = 'Велика';
    const radios = document.querySelectorAll('.size input')
    radios.forEach(radio => {
        radio.checked = 'default';
    })
    const existingIngrs = document.querySelectorAll('.ingr-wrapper input');
    existingIngrs.forEach(ingr => {
        ingredientsWrapper.removeChild(ingr);
    });
    const imgList = document.querySelectorAll('.dropped')
    imgList.forEach(img => {
        table.removeChild(img)
    })
    createNewPizza();
})

// ----------- Simulating the reset button only for credentials --------- //

resetButton.addEventListener('click', () => {
    const $ = id => document.getElementById(id);
    $('name').value = '';
    $('phone').value = '';
    $('email').value = '';
})

//--------- Converting ingredient inputs into the single one ----------// 

submitButton.addEventListener('click', () => {

    const existingIngrs = document.querySelectorAll('.ingr-wrapper input'),
        finalIngrList = document.createElement('input');
    finalIngrList.name = 'ingredient';
    finalIngrList.style.display = 'none';
    existingIngrs.forEach(ingr => {
        finalIngrList.value = finalIngrList.value.concat(ingr.value, ', ')
    });
    finalIngrList.value = finalIngrList.value.slice(0, -2);
    ingredientsWrapper.appendChild(finalIngrList);
})

// --------- Creating function for updating pizza price ---------//

function createNewPizza() {
    const pizza = new Pizza(sizes, ingredients, sauces)
    priceResult.value = `${pizza.getPrice()}грн.`;
}

// --------- Validation personal data form---------//

const loading = () => {
    let formValidation = false;
    for (let i = 0; i < form.elements.length; i++) {
        const el = form.elements[i]
        const mask = el.getAttribute('data-mask')
        if (mask) {
            el.onchange = validateInputs;
            formValidation = true;
        }
    }
    if (formValidation) {
        form.onsubmit = validateForm;
    }
    function validateInputs() {
        const mask = this.dataset.mask,
            value = this.value,
            result = value.search(mask);
        if (result === -1) {
            this.className = "error";
        } else {
            this.className = "valid";
        }
    }
    function validateForm() {
        let invalid = false;
        for (let i = 0; i < this.elements.length; ++i) {
            let el = this.elements[i];
            if (el.onchange != null) {
                el.onchange();
                if (el.className === "error") invalid = true;
            }
        }
        if (invalid) {
            alert("Form is invalid! Fix the entered data first");
            return false;
        }
    }
}

window.addEventListener("load", loading);

function Hamburger(size, stuffing) {
    try {
        this.size = size;
        this.stuffing = stuffing;
        if (size === undefined) {
            throw new HamburgerException('HamburgerException: ', 'No size given')
        } else if (stuffing === undefined) {
            throw new HamburgerException('HamburgerException: ', 'Stuffing is undefined')
        } else if (stuffing !== Hamburger.STUFFING_CHEESE && stuffing !== Hamburger.STUFFING_POTATO && stuffing !== Hamburger.STUFFING_SALAD) {
            throw new HamburgerException('HamburgerException: ', 'Invalid stuffing')
        } else if (size !== Hamburger.SIZE_LARGE && size !== Hamburger.SIZE_SMALL) {
            throw new HamburgerException('HamburgerException: ', 'Invalid size')
        }
    } catch (err) {
        return console.log(err.name + err.message);
    }
    var toppingsList = [];
    var addPrice = 0;
    var addCalories = 0;

    Object.defineProperties(this, {
        price: {
            get: function () {
                if (addPrice !== 0) {
                    return size[0] + stuffing[0] + addPrice
                } else {
                    return size[0] + stuffing[0]
                }
            },
            set: function (topping) {
                if (typeof topping === 'object') {
                    addPrice -= topping[0]
                } else {
                    addPrice += topping;
                }
            }
        },
        calories: {
            get: function () {
                if (addCalories !== 0) {
                    return size[1] + stuffing[1] + addCalories
                } else {
                    return size[1] + stuffing[1]
                }
            },
            set: function (topping) {
                if (typeof topping === 'object') {
                    addCalories -= topping[1]
                } else {
                    addCalories += topping;
                }
            }
        },
        toppings: {
            get: function () {
                return toppingsList
            },
            set: function (topping) {
                if (!toppingsList.includes(topping)) {
                    toppingsList.push(topping)
                } else {
                    var place = toppingsList.indexOf(topping)
                    newToppingsList = toppingsList.filter(function (item, index) {
                        return index !== place
                    })
                    toppingsList = newToppingsList
                }
            }
        }
    })
}

Hamburger.SIZE_SMALL = [50, 20];
Hamburger.SIZE_LARGE = [100, 40];
Hamburger.STUFFING_CHEESE = [10, 20];
Hamburger.STUFFING_SALAD = [20, 5];
Hamburger.STUFFING_POTATO = [15, 10];
Hamburger.TOPPING_MAYO = [20, 5];
Hamburger.TOPPING_SPICE = [15, 0];

Hamburger.prototype.addTopping = function (topping) {
    var toppingsList = this.getToppings();
    try {
        if (toppingsList.includes(topping)) {
            throw new HamburgerException('HamburgerException: ', 'Duplicate toppping')
        } else {
            this.toppings = topping;
            this.price = topping[0];
            this.calories = topping[1];
        }
    } catch (err) {
        return console.log(err.name + err.message);
    }
}

Hamburger.prototype.removeTopping = function (topping) {
    var toppingsList = this.getToppings();
    try {
        if (!toppingsList.includes(topping)) {
            throw new HamburgerException('HamburgerException: ', 'Topping is not exist')
        } else {
            this.toppings = topping
            this.price = topping
            this.calories = topping
        }
    } catch (err) {
        return console.log(err.name + err.message);
    }
}

Hamburger.prototype.getToppings = function () {
    return this.toppings
}

Hamburger.prototype.getSize = function () {
    return this.size
}

Hamburger.prototype.getStuffing = function () {
    return this.stuffing
}

Hamburger.prototype.calculatePrice = function () {
    return this.price 
}

Hamburger.prototype.calculateCalories = function () {
    return this.calories
}

function HamburgerException(name, message) {
    this.name = name;
    this.message = message;
}

HamburgerException.prototype = Object.create(Error.prototype);
HamburgerException.prototype.constructor = HamburgerException;


//---------- Initialization ----------//

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит? 
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE);
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length);

//--------- Errors ---------//

var h2 = new Hamburger();
var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE); 
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
h4.addTopping(Hamburger.TOPPING_MAYO);
h4.addTopping(Hamburger.TOPPING_MAYO); 

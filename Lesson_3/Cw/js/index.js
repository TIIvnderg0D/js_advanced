//варианты ф-ций

/*const user = function() {
    console.log(this);
}

function user() {
    console.log(this);
}

() => {

}

(function(){}())
(function(){})()

const user2 = function user() {}

//------ first ----------//

const user1 = {
    name:'Petya',
    surname: 'Ivanov',
    age: 35,
    speak: function(phone = 12, mobPhone, email) {
        console.group('Hello')
        console.log(`My name is ${this.name}`)
        console.log(`My surname is ${this.surname}`)
        console.log(`I am ${this.age}`)
        console.log(`My phone is ${phone}`)
        console.log(`My mobile phone is ${mobPhone}`)
        console.log(`My email is ${email}`)
        console.groupEnd('Bye')
    }
}

const user2 = {
    name:'Vasilyi',
    surname: 'Petrov',
    age: 46,
}*/

//user1.speak.bind(user2)()
//user1.speak.call(user2, '000000012')
//user1.speak.apply(user2, ['020202', '12345783', '1234@gmail.com'])

//user1.speak.bind(window)('000000')
//user1.speak('00000000').bind(window)

// ------- Second -------//

/*const arr = [1,4,6,8,9];

Array.prototype.multi = function(number) {
   return this.map( element => element * number ); 
}
console.log(arr.multi(5));
*/

// ------- Third -------//

/*function User(fullName) {
    this.fullName = fullName;
    Object.defineProperties(this, {
        firstName: {
            get: function() {
                return this.fullName.split(' ')[0]
            },
            set: function(name) {
                return this.fullName = `${name} ${this.lastName}`
            }
        },
        lastName: {
            get: function() {
                return this.fullName.split(' ')[1]
            },
            set: function(name) {
                return this.fullName = `${this.firstName} ${name}`
            }
        }
    })
}

const petya = new User("Petya Pyatochkin")

petya.firstName = 'Alisa'
petya.lastName = 'Petrovna'
console.log(petya)
console.log(petya.firstName)
console.log(petya.lastName)*/


// --------- Forth -----------//

/*let i = 0;

function func() {
    i++
    console.log(i)
}

func()
func()
func()*/


// --------- Fifth --------- //

const button = document.getElementById('buttonOne');

function random() {

    const arr = [];
    return function() {
      let number = Math.round(Math.random()*100+ 1);
      for (let i = 0; i <arr.length; i++ ) {
        if (arr[i] === number) {
            return false
        } 
      }
      arr.push(number)
      console.log(arr)
    } 
}

const a = random()

button.onclick = a;
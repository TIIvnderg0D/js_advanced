window.addEventListener('load', () => {
    const $id = id => document.getElementById(id),
        slides = document.querySelectorAll('.slide'),
        slideWidth = parseInt(getComputedStyle(slides[0], null).width.slice(0, -2)); // Getting the slider width
    let currentOffSet = 0, // current offset value
        isOriginal = true, // translate status
        activeSlide = 0,  // active slide
        offsetsWidth = 0; // bordering offset range

    // ------------- Setting the handler for 'Next' button ------------- //
    $id('next').addEventListener('click', () => {
        if (activeSlide === slides.length - 1) {  // if current slide is the last, translating the offset to all except the last
            offsetsWidth += slideWidth * slides.length;
            slides.forEach((slide, index) => {
                if (index !== activeSlide) {
                    slide.style.transform = `translateX(${offsetsWidth}px)`;
                }
            })
            activeSlide = -1;
            isOriginal = false;
        } else if (activeSlide === slides.length - 2 && !isOriginal) {  // if it was translated and current slide is one before last
            slides[slides.length - 1].style.transform = `translateX(${offsetsWidth}px)`;
        }
        activeSlide++
        currentOffSet -= slideWidth;
        $id('slider').style.transform = `translateX(${currentOffSet}px)`;  // changing slides 
    })

    // ------------- Setting the handler for 'Previous' button ------------- //
    $id('previous').addEventListener('click', () => {  // if current slide is the first, translating the offset to all except the first
        if (activeSlide === 0) {
            offsetsWidth -= slideWidth * slides.length;
            slides.forEach((slide, index) => {
                if (index !== activeSlide) {
                    slide.style.transform = `translateX(${offsetsWidth}px)`;
                }
            })
            activeSlide = slides.length
            isOriginal = false;
        } else if (activeSlide === 1 && !isOriginal) {  // if it was translated and current slide is one before first
            slides[0].style.transform = `translateX(${offsetsWidth}px)`;
        }
        activeSlide--
        currentOffSet += slideWidth;
        $id('slider').style.transform = `translateX(${currentOffSet}px)`;  // changing slides 
    })
})
